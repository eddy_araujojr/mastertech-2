package appteste;

public class PessoaFisica extends Pessoa{
	
	private String rg;
	
	@Override
	public final String apresentar() {
		return super.apresentar() + ", RG: " + rg;
	}

}
