package appteste;

public abstract class SerVivo {
	private int idade;
	
	public void envelhecer() {
		idade = idade + 1;
	}
	
	public abstract String apresentar();
}
