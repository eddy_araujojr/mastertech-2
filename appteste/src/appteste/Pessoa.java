package appteste;

import java.io.Closeable;
import java.io.IOException;

public class Pessoa extends SerVivo implements Andavel, Closeable{
	
	//Método implementado para interface Andavel
	public void Andar() {
		// TODO andar
		return;
	}
	//Método implementado para interface Closable
	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		return;
	}
	
	public String apresentar() {
		return "Nome: " +  nome;
	}
	
	private int id;
	private String nome;
	
	public int getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		if(!nome.equals(""))
			this.nome = nome;
	}
}
