package appteste;

public class Coisa {
	//Atributos
	public String nome;
	public int numero;
	//Métodos
	public String nomeDaCoisa() {
		return "Olá, meu nome é " + nome;
	}
	//Construtores
	//Construtor 1 (vazio)
	public Coisa() {
	}
	//Construtor 2 (sobrecarga)
	public Coisa(String nome, int numero) {
		this.nome = nome;
		this.numero = numero;
	}
}
