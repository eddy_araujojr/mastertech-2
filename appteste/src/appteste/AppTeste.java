package appteste;

public class AppTeste {
	
	public static void main (String[] args) {
		
		//declarando e atribuindo valor em vários tipos de variáveis
		//tipos por valor
		byte b = 0;
		int i = 5;
		long l = 9L;
		short s = 3;
		double d = 9.9;
		float f = 3.5f;
		boolean bo = false;
		char c = 'c';
		//tipos por referência
		String veiculo = "Carro";
		Object o = new Object();
		Coisa umaCoisa = new Coisa();
		//vetores
		int[] vetor1 = new int[5];
		int[] vetor2 = {1, 2, 3, 4};
		int[][] vDeVetor = new int[5][4];
		int[][][] vDvDeVetor = new int[5][4][3];
		int[][][][] vDvDvDeVetor = new int[5][4][3][5];
		int[][] vDeVetor2 = {{1, 2, 3}, {4, 5}};
		int[][] vDeVetor3 = {new int[]{1, 2}, new int[]{3, 4, 5}};
		
		//Experiência tipo por referência 1
		o = i;
		System.out.println("int i = " + i);
		System.out.println("Object o = " + o);
		mudaObjeto(i, o);
		System.out.println("Chamado 'mudaObjeto'. Resultado:");
		System.out.println("int i = " + i);
		System.out.println("Object o = " + o);
		System.out.println("A função 'mudaObjeto' trocou a referência dentro do método.");
		//Experiência tipo por referência 2
		umaCoisa.numero = 10;
		System.out.println("Coisa.numero = " + umaCoisa.numero);
		mudaObjeto(i, umaCoisa);
		System.out.println("Chamado 'mudaObjeto'. Resultado:");
		System.out.println("Coisa.numero = " + umaCoisa.numero);
		System.out.println("A função 'mudaObjeto' atualizou atributos do objeto recebido.");
		
		//Estruturas de Decisão
		//if else
		if (i >= 18) {
			System.out.println("Entrada autorizada");
		}
		else if (i > 15 && i < 18) {
			System.out.println("Entrada somente com os pais.");
		}
		else
			System.out.println("Não autorizado.");
		
		//switch case
		System.out.println("Você precisa de...");
		switch (veiculo) {
		case "caminhao":
			System.out.println("disel");
			break;
		case "Moto":
		case "Carro":
			System.out.println("gasolina");
			break;
		case "":
			System.out.println("capim");
			break;
		default:
			System.out.println("alguma outra coisa");
			break;
		}
		
		//Estruturas de repetição
		//while
		while (i < 10) {
			System.out.println(i);
			i++;
		}
		//do while
		do {
			System.out.println(i);
			i++;
		} while (i < 10);
		//for
		for (int j = 0; j < 5; j++) {
			System.out.println(j);
		}
		//for each
		for (String string : args) {
			System.out.println(string);
		}
		
		//enum
		Continente cnt = Continente.America;
		
		//Classe Objeto
		Coisa eu = new Coisa();
		eu.nome = "Eddy";
		System.out.println(eu.nomeDaCoisa());
		
	}
	
	public static void mudaObjeto(int i, Object o) {
		i = i + 5;
		o = Integer.parseInt(o.toString()) + 9;
	}
	
	public static void mudaObjeto(int i, Coisa o) {
		i = i + 5;
		o.numero = o.numero + 9;
	}
}

/*public static void mainCoisa (String[] args) {		
	String a = "";
	if(a.getClass() == String.class) {
		System.out.println("true");
	}
	else
		System.out.println(false);
	
	Coisa uma = new Coisa();
	Coisa outra = new Coisa();
	
	if(uma == outra) {
		System.out.println("Uma Coisa é igual a outra Coisa");
	}
	else {
		System.out.println("Uma coisa não é igual a outra coisa");
	}
	
	if(uma.getClass() == Coisa.class) {
		System.out.println("Uma coisa é uma coisa");
	}
	else {
		System.out.println("Uma coisa não é uma coisa");
	}
	
	if(outra.getClass() == Coisa.class) {
		System.out.println("Outra coisa é uma coisa");
	}
	else {
		System.out.println("Outra coisa não é uma coisa");
	}
}*/
