package apppolimorphtest;

import java.util.ArrayList;
import java.util.List;

/**
 * AppPolimorph
 */
public class AppPolimorph {

    public static void main(String[] args) {
        
        //Exemplo 1 - herança
        System.out.println("Exemplo 1 - herança");
        //Aqui temos 6 linhas
        List<Pessoa> pessoas = new ArrayList<Pessoa>();
        pessoas.add(new PessoaFisica("Eddy")); //Adicionamos na lista de "Pessoas" "Pessoas Físicas" 
        pessoas.add(new PessoaJuridica("Max Power")); //e "Pessoas Jurídicas"
        pessoas.add(new PessoaJuridica("Mastertech"));
        pessoas.add(new PessoaJuridica("Itau")); 
        //e seria + 1 linha para cada instância (sem falar em declaração de variáveis)
        apresentacao(pessoas); //chamamos um método que aceita uma lista do supertipo Pessoas...
        //...(independente qual seja o subtipo)

        /*Aqui foram 8 linhas
        Pessoa pf = new PessoaFisica("Eddy");
        Pessoa pj = new PessoaJuridica("Max Power");
        Pessoa pj2 = new PessoaJuridica("Mastertech");
        Pessoa pj3 = new PessoaJuridica("Itau");
        System.out.println(pf.seApresentar());
        System.out.println(pj.seApresentar());
        System.out.println(pj2.seApresentar());
        System.out.println(pj3.seApresentar());
        e seriam + 2 para cada instância*/

        //Exemplo 2 - interfaces
        System.out.println("Exemplo 2 - interfaces");
        //Para utilizar o mesmo objeto e mudar o tipo (do genérico para o mais específico)...
        PessoaJuridica pj1 = (PessoaJuridica)pessoas.get(1); //... utilizamos conversão (casting)
        Conglomerado conglom = new Conglomerado();
        conglom.add(pj1);
        conglom.add((PessoaJuridica)pessoas.get(2)); //conversão (casting) direto na inserção
        conglom.add((PessoaJuridica)pessoas.get(3)); //conversão (casting) direto na inserção
        //chamamos o método que aceita um tipo Fornecedor (Interface)...
        mostrarFornecedores(pj1); //... passando uma PessoaJuridica
        mostrarFornecedores(conglom); //... e passando um Conglomerado
    }

    //+ quatro linhas - Método que aceita uma lista de Pessoas
    public static void apresentacao(List<Pessoa> pessoas) {
        for (Pessoa p : pessoas) {
            System.out.println(p.seApresentar() + ". " + p.dizerTipo());
        }
    }

    //Método que aceita um Fornecedor
    public static void mostrarFornecedores(Fornecedor fornecedor) {
        System.out.println(fornecedor.dizerOqueFornece());
    }
}