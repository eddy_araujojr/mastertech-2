package apppolimorphtest;

/**
 * Pessoa - Classe Abstrata
 */
public abstract class Pessoa {

    //Encapsulamento - Protegendo meus atributos
    private static int contador;
    private int id;
    private String nome;

    //Construtor vazio de Pessoa
    public Pessoa() {
        Pessoa.contador++;
        this.id = Pessoa.contador;
    }

    //Sobrescrita de Construtor
    public Pessoa(String nome) {
        this();
        this.nome = nome;
    }

    /**
     * @return the id
     * Para este atribudo, só permiti a consulta
     */
    public final int getId() {
        return id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }
    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        if(!nome.isEmpty())
            this.nome = nome;
    }

    //Metodo implementado (caso uma classe extenda Pessoa, NÃO precisa construir este método)
    public String seApresentar() {
        return "Olá, eu sou " + this.nome;
    }

    //Metodo NÃO implementado (caso uma classe extenda Pessoa, PRECISA construir este método)
    public abstract String dizerTipo();
}