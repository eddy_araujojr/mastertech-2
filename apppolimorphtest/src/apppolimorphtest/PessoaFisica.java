package apppolimorphtest;

/**
 * PessoaFisica
 */
public class PessoaFisica extends Pessoa {

    private String cpf;

    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    //Implementando (construíndo) método que possui implementação na classe abstrata (sobreescrevendo)
    @Override
    public String seApresentar() {
        //O método, a pesar de sobrescrever a superclasse, chama internamente o método desta através do comando "super."
        return super.seApresentar() + "; CPF: " + cpf;
    }

    //Implementando (construíndo) método que está definido na classe abstrata como abstrato (sem implementação)
    @Override
    public String dizerTipo() {
        return "Eu sou uma pessoa física";
    }

    //Sobrescrevendo construtor chamando o construtor da superclasse
    public PessoaFisica(String nome){
        super(nome);
    }
}