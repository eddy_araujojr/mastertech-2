package apppolimorphtest;

/**
 * PessoaJuridica
 */
public class PessoaJuridica extends Pessoa implements Fornecedor{

    private String cnpj;
    private String oqueFornece;

    /**
     * @return the cnpj
     */
    public String getCnpj() {
        return cnpj;
    }
    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    /**
     * @return the oqueFornece
     */
    public String getOqueFornece() {
        return oqueFornece;
    }
    /**
     * @param oqueFornece the oqueFornece to set
     */
    public void setOqueFornece(String oqueFornece) {
        this.oqueFornece = oqueFornece;
    }

    //Implementando (construíndo) método que possui implementação na classe abstrata (sobreescrevendo)
    @Override
    public String seApresentar() {
        return super.seApresentar() + "; CNPJ: " + this.cnpj;
    }

    //Implementando (construíndo) método que está definido na classe abstrata como abstrato (sem implementação)
    @Override
    public String dizerTipo() {
        return "Eu sou uma pessoa jurídica";
    }

    //Sobrescrevendo construtor chamando o construtor da superclasse
    public PessoaJuridica(String nome){
        super(nome);
    }

    //Implementando (construíndo) método que está definido na interface
    public String dizerOqueFornece(){
        //Detalhe: chamando a propriedade da superclasse (pois o atributo é privado)
        return super.getNome() + ". Esta empresa fornece " + oqueFornece;
    }
}