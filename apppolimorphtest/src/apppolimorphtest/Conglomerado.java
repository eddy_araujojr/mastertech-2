package apppolimorphtest;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
/**
 * Conglomerado - Implementa duas interfaces: 
 * Fornecedor (criada por aqui) 
 * Iterable (biblioteca do java para classes que podem ser iteradas, ou seja, passam por foreach)
 */
public class Conglomerado implements Fornecedor, Iterable<PessoaJuridica>{

    private List<PessoaJuridica> empresas;
    private String oqueFornece;

    public Conglomerado() {
        empresas = new ArrayList<PessoaJuridica>();
    }

    /**
     * @return the oqueFornece
     */
    public String getOqueFornece() {
        return oqueFornece;
    }
    /**
     * @param oqueFornece the oqueFornece to set
     */
    public void setOqueFornece(String oqueFornece) {
        this.oqueFornece = oqueFornece;
    }

    //método da interface Fornecedor
    public String dizerOqueFornece(){
        int index = 0;
        String nome = "";
        //Foreach (OBRIGADO INTERFACE ITERABLE)
        for (PessoaJuridica pj : this) { //"this" siginifica este objeto
            if(pj.getNome().length() > 0)
                nome = nome + pj.getNome().charAt(0);
            index++;
        }
        return nome + ". Este conglomerado (de " 
            + index + " empresas ) fornece " + oqueFornece;
    }

    //método da interface Iterable
    public Iterator<PessoaJuridica> iterator(){
        return empresas.iterator();
    }

    public boolean add(PessoaJuridica p){
        return empresas.add(p);
    }

}